Tag: uses-gbp-conf
Severity: classification
Certainty: possible
Check: version-control/git-buildpackage
Info: The package appears to use git-buildpackage (gbp) for package maintenance,
 as indicated by the presence of a gbp.conf configuration file.
 .
 This is an informational tag to help classify packages using git-buildpackage
 for maintenance. Git-buildpackage is a suite of tools that helps with
 maintaining Debian packages in Git repositories.
 .
 The presence of gbp.conf indicates the package follows git-buildpackage
 workflows and conventions for Debian package maintenance.
Ref: gbp(1), git-buildpackage(1)
