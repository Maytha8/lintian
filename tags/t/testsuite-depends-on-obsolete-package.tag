Tag: testsuite-depends-on-obsolete-package
Severity: warning
Check: testsuite
Explanation: The testsuite depends on a package that has been superseded.
 If the superseded package is part of an ORed group, it should not be
 the first package in the group.
