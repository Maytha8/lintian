Tag: testsuite-ored-depends-on-obsolete-package
Severity: info
Check: testsuite
Explanation: The testsuite depends on an ORed group of packages which includes
 a package that has been superseded.
