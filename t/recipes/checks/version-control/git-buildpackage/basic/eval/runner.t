#!/usr/bin/perl

use strict;
use warnings;

use Test::More;

# Basic test for git-buildpackage configuration detection
my $testname = 'version-control/git-buildpackage/basic';

# Check if gbp.conf exists in the test package
ok(-f "t/recipes/checks/version-control/git-buildpackage/basic/build-spec/debian/gbp.conf",
    'gbp.conf exists');

# Check if the tag file exists
ok(-f "tags/v/version-control/uses-gbp-conf.tag",
    'uses-gbp-conf tag is defined');

done_testing;
